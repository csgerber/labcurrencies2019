package edu.uchicago.gerber.labcurrencies2019;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

public class SplashActivity extends AppCompatActivity {
    //url to currency codes used in this application
  //  public static final String URL_CODES = "https://gerber.cs.uchicago.edu/res/rates.json";
    public static final String KEY_ARRAYLIST = "key_arraylist";
    //ArrayList of currencies that will be fetched and passed into MainActivity
    private ArrayList<String> mCurrencies;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

       JSONObject jsonObject = readRawTextFile(this, R.raw.rates);
       processTextFile(jsonObject);

    }

    private void processTextFile(JSONObject jsonObject){
        try {
            if (jsonObject == null) {
                throw new JSONException("no data available.");
            }
            Iterator iterator = jsonObject.keys();
            String key = "";
            mCurrencies = new ArrayList<>();
            while (iterator.hasNext()) {
                key = (String)iterator.next();
                mCurrencies.add(key + " | " + jsonObject.getString(key));
            }
            Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
            mainIntent.putExtra(KEY_ARRAYLIST, mCurrencies);
            startActivity(mainIntent);
            finish();
        } catch (JSONException e) {
            Toast.makeText(
                    SplashActivity.this,
                    "There's been a JSON exception: " + e.getMessage(),
                    Toast.LENGTH_LONG
            ).show();
            e.printStackTrace();
            finish();
        }
    }

    private static JSONObject readRawTextFile(Context ctx, int resId)
    {
        InputStream inputStream = ctx.getResources().openRawResource(resId);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            return null;
        }
        JSONObject jsonObject = null;
        try {
             jsonObject = new JSONObject(text.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }



}
